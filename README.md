# MariaDB Physical Backup and Restore

[[_TOC_]]

Repositori ini berisi tutorial untuk melakukan physical backup dan restore database MariaDB pada OS Ubuntu 20.04 LTS.

> :bulb: Baca [sekilas tentang backup dan restore pada MariaDB](01-sekilas-backup-mariadb.md)

## 1. Persiapan physical backup

Ada beberapa hal yang perlu dipersiapkan sebelum melakukan backup database MariaDB, yaitu:

### 1.1 Memastikan tool `mariabackup` sudah terinstall

Pastikan tool `mariabackup` sudah terinstall pada server MariaDB. Jika belum, silahkan install terlebih dahulu dengan perintah berikut:

```bash
sudo apt-get install mariabackup
```

> **Tips dan Peringatan:**
>
> - :bulb: Sebaiknya install dari [repositori resmi mariadb](https://mariadb.org/download/?t=repo-config)
> - :warning: Pastikan versi `mariabackup` yang terinstall sama dengan versi MariaDB yang terinstall

### 1.2 Membuat user backup

Ketika membuat physical backup, tool `mariabackup` membutuhkan user yang memiliki privilege `RELOAD` dan `PROCESS`. Oleh karen itu kita perlu membuat user backup yang memiliki privilege tersebut:

Pertama-tama kita masuk ke MariaDB dengan user `root`:

```bash
mysql -u root -p
```

Setelah itu kita buat user backup dengan nama `backup` dan beri password yaitu `password`:

```sql
CREATE USER 'backup'@'localhost' IDENTIFIED BY 'password';
```

Setelah itu kita berikan privilege `RELOAD` dan `PROCESS` ke user `backup`:

```sql
GRANT RELOAD, PROCESS ON *.* TO 'backup'@'localhost';
```

## 2. Melakukan physical backup

Tentukanlah sebuah direktori yang akan digunakan untuk menyimpan backup (misalnya: `/backup`) dan eksekusi perintah berikut:

```bash
sudo mariabackup --backup --target-dir=/backup --user=backup --password=password
```

Jika perintah diatas berhasil dijalankan, maka file-file backup akan tersimpan pada direktori yang sudah kita tentukan (`/backup`).

## 3. Melakukan restore database

Langkah awal melakukan restore adalah dengan mengeksekusi perintah dibawah ini agar file-file backup menjadi siap untuk di-restore:

```bash
sudo mariabackup --prepare --target-dir=/backup
```

Selanjutnya kita hentikan service MariaDB dengan perintah:

```bash
sudo systemctl stop mariadb
```

Kemudian kita harus menghapus semua data yang ada pada `datadir` (default: `/var/lib/mysql`):

```bash
sudo rm -rf /var/lib/mysql/*
```

> :warning: Pastikan kembali bahwa direktori `/var/lib/mysql` sudah benar-benar kosong setelah perintah diatas dijalankan.

Barulah kita bisa melakukan restore database dengan perintah berikut:

```bash
sudo mariabackup --copy-back --target-dir=/backup
```

Sebelum menghidupkan kembali service MariaDB, kita perlu memastikan agar direktori `/var/lib/mysql` memiliki ownership `mysql:mysql` dengan mengeksekusi perintah:

```bash
sudo chown -Rf mysql:mysql /var/lib/mysql
```

Dan sebagai langkah terakhir, kita hidupkan kembali service MariaDB dengan perintah:

```bash
sudo systemctl start mariadb
```
