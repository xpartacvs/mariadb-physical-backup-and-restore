# Sekilas Tentang Backup dan Restore pada MariaDB

Pada konteks backup dan restore, MariaDB menyediakan dua cara untuk melakukannya, yaitu: _Logical_ dan _Physical_.

## Logical Backup

Logical backup adalah cara backup yang dilakukan dengan cara mengambil data dari database dan menyimpannya dalam bentuk file _(biasaya *.sql)_ yang dapat dibaca oleh manusia _(text-based)_. Untuk melakukan logical backup biasanya dengan menggunakan perintah `mysqldump`.

## Physical Backup

Sedangkan physical backup adalah cara backup yang dilakukan dengan cara mengambil keseluruhan data dari mariadb dan menyimpannya dalam bentuk binnary yang tidak bisa dibaca oleh manusia. Pada mariadb, perintah yang biasanya digunakan untuk melakukan physical backup adalah `mariabackup`.

## Pro vs Cons

Berikut ini adalah tabel perbandingan keunggulan dan kekurangan antara logical backup dan physical backup:

|                        | **Logical**                | **Physical**               |
| :---                   | :---                       | :---                       |
| **Execition time**     | Slower than physical       | Faster than logical        |
| **File format**        | Text based                 | Binary                     |
| **File size**          | Bigger than physical       | Smaller than logical       |
| **Version portable**   | Yes                        | No                         |
